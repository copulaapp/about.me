---
date: '2018-12-01'
title: 'Ochulo Travels'
image: ''
github: ''
external: 'https://ochulotravel.com/'
tech:
  - AKKA
  - Scala
  - Play
  - Amadeus
show: 'true'
---

Ochulo Travels is a portal providing flight ticket, hotel accomdations, visa assistance and car hiring service.