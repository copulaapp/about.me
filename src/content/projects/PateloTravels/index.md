---
date: '2018-12-01'
title: 'Patelo Travels'
image: ''
github: ''
external: 'https://patelotravels.com/'
tech:
  - AKKA
  - Scala
  - Play
  - Amadeus
show: 'true'
---

Patelo Travels is a portal providing flight ticket, hotel accomdations, visa assistance and car hiring service.