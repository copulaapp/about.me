---
date: '2017-06-01'
title: 'Vecta Travels'
cover: './vectatravel.png'
# github: 'https://github.com/bchiang7/v3'
external: 'https://www.vectatravels.com/'
tech:
  - Scala
  - Play Framework
  - Java
  - Amadeus API
---

Vecta Travels is a world class provider of traveling experience with concierge services, car hire, hotel accommodation, travel insurance, visa assistance and affordable flight tickets.
