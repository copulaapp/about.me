---
date: '2017-11-01'
title: 'StoryartApp'
cover: './storyartapp.png'
github: ''
external: 'https://storyartapp.com/'
tech:
  - Android
  - Java
  - C
  - JAX-RS, JERSEY
---

An Android media editing application, to edit picture, apply filters, text with styling, create movie picture with songs, and more. And easily share as Instagram, WhatsApp or Facebook story. Storyart as over 10K downloads on Playstore. 