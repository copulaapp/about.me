---
date: '2016-02-17'
title: 'CopulaApp'
cover: './copulaapp.png'
# github: 'https://github.com/bchiang7/halcyon-site'
external: 'http://copulaapp.com/'
tech:
  - Android
  - Java
  - C
  - JAX-RS, JERSEY
---

An Android application that allows offline streaming and sharing of media on nearby Android devices by peering with them.