---
title: 'Hi, my name is'
name: 'Elias Igbalajobi'
subtitle: 'I build things for the web & mobile.'
contactText: 'Get In Touch'
---

I'm a software engineer based in Lagos, NG specializing in developing cutting edge web and mobile applications. Currently working at [Oblangata](https://www.oblangata.com/) as an iOS Engineer.
