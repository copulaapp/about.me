---
date: '2017-11-01'
title: 'Senior Android Engineer'
company: 'Copula'
location: 'Lagos, NG'
range: 'July - Dec 2015'
url: 'https://www.copulaapp.com/'
---

- Responsible for the design & development of Copula, an offline peer-to-peer media streaming and download application (Currently available on Google play store)
- Setup local HTTP Progressive Audio/Video file streaming service for the Copula app
- Implemented personalized push notification service, which increased app usage by over 36%
