---
date: '2014-06-01'
title: 'Senior Android Engineer'
company: 'Terrax'
location: 'Abuja, NG'
range: 'June 2014 - June 2015'
# url: 'https://web.northeastern.edu/scout/'
---

- Developed and shipped Veetu e-commerce Android mobile application for airtime and data purchase
