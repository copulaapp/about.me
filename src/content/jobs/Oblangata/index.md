---
date: '2018-07-01'
title: 'Senior iOS Engineer'
company: 'Oblangata'
location: 'Lagos, NG'
range: 'July 2018 - Present'
url: 'https://www.oblangata.com/'
---


- Developed and shipped native iOS application for offline e-commerce service using SMS API service
- Worked closely with web backend engineer in the development of scalable offline SMS API service
- Write modern, performant, maintainable code for a diverse array of client and internal projects