---
title: 'About Me'
avatar: './me.jpg'
skills:
  - Programming - Java, C, Swift, Kotlin
  - Framework - Android SDK, iOS SDK, JAX-RS, Jersey
  - Databases - MySQL, Cassandra, Redis, Memcached
  - Web Servers - Nginx, Apache Tomcat, Glassfish
  - Developer Operation - AWS, GCP, Linode
  - Operating System - Linux, MacOS
  - Version Control - Git
  - Container - Docker
  - Testing Frameworks - JUnit (JAVA)
  - Platform - AWS(EC2, S3, SNS/SES, Route53)
---


I am a software engineer with expertise in mobile and web applications development. I build cutting-edge web and mobile apps ranging from e-commerce, multimedia, and travel booking portals which are intuitive, and also being scalable and efficient behind the scenes. I enjoy constantly learning and improving my skills with the technologies that keep evolving in this ever-changing field!

I also help in facilitating and mentoring beginner and experienced Android developer with Andela's Google Africa Scholarship

Here's a few things I'm experienced with:
