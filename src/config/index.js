module.exports = {
  siteTitle: 'Elias Igbalajobi | Front-End Software Engineer',
  siteDescription:
    'Web and Mobile Software engineer based in Lagos, NG who specializes in developing exceptional, high-quality websites and applications.',
  siteKeywords:
    'Elias Igbalajobi, Elias, Igbalajobi, heeleeaz, software engineer, mobile engineer, web developer, android software engineer, ios software, ios software developer, android software engineer',
  siteUrl: 'https://ielias.me',
  siteLanguage: 'en_US',

  // googleVerification: 'DCl7VAf9tcz6eD9gb67NfkNnJ1PKRNcg8qQiwpbx9Lk',

  name: 'Elias Igbalajobi',
  location: 'Lagos, NA',
  email: 'heeleeaz@gmail.com',
  socialMedia: [
    {
      name: 'Github',
      url: 'https://github.com/heeleeaz/',
    },
    {
      name: 'Linkedin',
      url: 'https://www.linkedin.com/in/elias-igbalajobi/',
    },
    {
      name: 'Instagram',
      url: 'https://www.instagram.com/heeleeaz/',
    },
    {
      name: 'Twitter',
      url: 'https://twitter.com/heeleeaz',
    },
  ],

  nav: [
    {
      name: 'About',
      url: '#about',
    },
    {
      name: 'Experience',
      url: '#jobs',
    },
    {
      name: 'Work',
      url: '#projects',
    },
    {
      name: 'Contact',
      url: '#contact',
    },
  ],

  twitterHandle: '@heeleeaz',
  googleAnalyticsID: 'UA-121293848-2',

  headerHeight: 100,

  greenColor: '#64ffaa',
  navyColor: '#0a192f',

  srConfig: (delay = 200) => {
    return {
      origin: 'bottom',
      distance: '20px',
      duration: 500,
      delay,
      rotate: { x: 0, y: 0, z: 0 },
      opacity: 0,
      scale: 1,
      easing: 'cubic-bezier(0.645, 0.045, 0.355, 1)',
      mobile: true,
      reset: false,
      useDelay: 'always',
      viewFactor: 0.25,
      viewOffset: { top: 0, right: 0, bottom: 0, left: 0 },
    };
  },
};
